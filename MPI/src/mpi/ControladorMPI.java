/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package mpi;

import java.awt.HeadlessException;
import java.io.*;
import javax.swing.JFileChooser;
import java.net.URISyntaxException;
import javax.swing.JOptionPane;

/**
 *
 * @author Cristobal Rios
 */
public class ControladorMPI {

    //Metodo para subir el archivo
    public String Path() {
        String path = "";

        try {
            File archive = route();
            //Controlamos la subida del archivo
            if (archive != null) {
                path = archive.getName(); //Obtener solo el nombre especifico del archivo
            } else {
                JOptionPane.showMessageDialog(null, "No se puede encontrar el archivo");
            }

        } catch (HeadlessException e) {
            System.err.println("Existe un error al subir el archivo : " + e);
        }
        return path;
    }

    //Metodo para compilar el codigo en C
    public Process Compilar(String path, String nameFile) throws IOException {
        Process process = null;
        try {
            if (path.equals(" ")) {
                JOptionPane.showMessageDialog(null, "La subido del archivo es erronea. Intente de nuevo!!");
            } else {
                //Variable que simula el comando en la terminal
                String[] comand = {"mpicc", "-o", nameFile, path};
                //Runtime.getRuntime().exec(cmd);
                System.out.println("El path es : " + path);
                //String comand = "sh -c ls";
                //Llamamos a la ejecucion
                //process = Runtime.getRuntime().exec(comand,null,new File("/home/usuario/Descargas/Codigos/"));
                JOptionPane.showMessageDialog(null, "Escoga la ruta de guardado (Seleccione el mismo archivo compilado)");
                JFileChooser upFile = new JFileChooser();
                upFile.showOpenDialog(null);
                File routearch = upFile.getCurrentDirectory();
                process = Runtime.getRuntime().exec(comand, null, new File(routearch.getAbsolutePath()));
                System.out.println("SE COMPILO");

            }
        } catch (IOException e) {
            System.err.println("Ups! Ocurrio un error : " + e);
        }
        return process;
    }

    //Ejecucion
    public String Ejecutar() throws IOException {
        String outline = "";
        try {
            File routeE = route();
            //Controlamos la subida del archivo
            if (routeE != null) {
                String path = routeE.getName(); //Obtener solo el nombre especifico del archivo

                //Calculamos el numero de PROCESADORES
                /*String nmr [] = {"lscpu", "-p", "|", "egrep", "-v", "'^#'", "|", "sort", "-u", "-t,", "-k", "2,4", "|", "wc", "-l"};
                
                Process proceso = Runtime.getRuntime().exec(nmr, null, null);
                BufferedReader reader = new BufferedReader(new InputStreamReader(proceso.getInputStream()));
                String out = "";
                String line = "";
                while ((line = reader.readLine()) != null) {
                    System.out.println(line);
                    out += line + "\n";
                }*/
                int m = Runtime.getRuntime().availableProcessors(); //Numero de procesadores
                int m1 = m;
                System.out.println("Ruta : " + path);
                System.out.println("Procesadores : " + m);

                //SIMULAMOS
                //EJECUTAMOS EL COMANDO DE EJECUCION DE MPI
                //String [] comand = {"mpirun"," -np ", String.valueOf(m1), " ./"+path}; 
                String[] comand = {"bash", "-c", "mpirun -np " + String.valueOf(m1) + " ./" + path};
                Process process = Runtime.getRuntime().exec(comand);
                //System.out.println("SE COMPILO completamente");
                //process = Runtime.getRuntime().exec("ls");
                BufferedReader read = new BufferedReader(new InputStreamReader(process.getInputStream()));
                String resultadobr;

                while ((resultadobr = read.readLine()) != null) {
                    outline = outline + resultadobr + " \n";
                }

                System.out.println(outline); //Mostramos los rsultados y exportamos

            } else {
                JOptionPane.showMessageDialog(null, "No se puede encontrar el archivo");
            }

        } catch (HeadlessException e) {
            JOptionPane.showMessageDialog(null, "Error " + e);
        }
        return outline;
    }

    //Obtener ruta
    public File route() {
        JFileChooser route = new JFileChooser();
        route.showOpenDialog(null);
        File routearch = route.getSelectedFile();
        return routearch;
    }

    public String OUT(Process process) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String out = "";
        String line = "";
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
            out += line + "\n";
        }
        return out;
    }

    //Metodo para valorar la subida del archivo
    public boolean upFile() {
        try {
            JFileChooser upFile = new JFileChooser();
            upFile.showOpenDialog(null);
            File archive = upFile.getSelectedFile();
            System.out.println(archive.getAbsolutePath());
            return true;
        } catch (HeadlessException e) {
            System.out.print(e);
            return false;
        }
    }

    public String leerTxt(String direccion) { //direccion del archivo

        String texto = "";

        try {
            BufferedReader bf = new BufferedReader(new FileReader(direccion));
            String temp = "";
            String bfRead;
            while ((bfRead = bf.readLine()) != null) {
                //haz el ciclo, mientras bfRead tiene datos
                temp = temp + bfRead; //guardado el texto del archivo
            }

            texto = temp;

        } catch (IOException e) {
            System.err.println("No se encontro archivo");
        }

        return texto;

    }

    //Abrir cosas desde java
    public void openChrome() {
        try {
            if (java.awt.Desktop.isDesktopSupported()) {
                java.awt.Desktop desktop = java.awt.Desktop.getDesktop();
                if (desktop.isSupported(java.awt.Desktop.Action.BROWSE)) {
                    try {
                        java.net.URI url = new java.net.URI("https://www.youtube.com/watch?v=oNTjr917cMw");
                        desktop.browse(url);
                    } catch (URISyntaxException | IOException e) {
                        System.err.println("Error " + e);
                    }
                }
            }
        } catch (Exception e) {
        }
    }
}
